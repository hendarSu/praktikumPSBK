-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Apr 2016 pada 03.04
-- Versi Server: 5.6.24
-- PHP Version: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `modul5`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemilih`
--

CREATE TABLE IF NOT EXISTS `pemilih` (
  `id_pemilih` varchar(10) NOT NULL,
  `nama_pemilih` varchar(70) NOT NULL,
  `no_hp_pemilih` varchar(14) NOT NULL,
  `email_pemilih` varchar(50) NOT NULL,
  `status_vote_pemilih` varchar(20) NOT NULL,
  `status_pemilih` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pemilih`
--

INSERT INTO `pemilih` (`id_pemilih`, `nama_pemilih`, `no_hp_pemilih`, `email_pemilih`, `status_vote_pemilih`, `status_pemilih`) VALUES
('103040001', 'A', '085720010001', 'a@gmail.com', 'belum', 1),
('103040002', 'B', '085720010002', 'b@gmail.com', 'belum', 1),
('103040003', 'C', '085720010003', 'c@gmail.com', 'belum', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pemilih`
--
ALTER TABLE `pemilih`
  ADD PRIMARY KEY (`id_pemilih`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
